if [ "$1" = "-h" ] || [ "$1" = "--help" ]; then
    echo "Usage: ./convert.sh [planfile] [path-to-interop-server]"
    exit;
fi

if [ "$#" -ne 2 ]; then
    echo "Incorrect amount of arguments."
    exit;
fi

filepath=$(realpath $1)
serverpath=$(realpath $2)
echo $filepath
echo $serverpath

cd $(dirname $0)

echo "FILE_LOCATION=$filepath" > .env
echo "SERVER_LOCATION=$serverpath" >> .env

docker-compose run interop-server python3 qgc2interop.py plan.plan $serverpath
docker stop qgc2interop_interop-db_1
