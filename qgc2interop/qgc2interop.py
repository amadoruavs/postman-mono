import requests
import json
import random

from typing import List
from argparse import ArgumentParser

parser = ArgumentParser()
parser.add_argument("--min-height",
                    help="Minimum height for fly zone in feet above MSL. Defaults to 100ft MSL.",
                    default="100")
parser.add_argument("--max-height",
                    help="Maximum height for fly zone in feet above MSL. Defaults to 750ft MSL.",
                    default="750")
parser.add_argument("planfile", help="The QGC .plan file to convert to an Interop mission.")
parser.add_argument("interop_path", help="Location of the Interop Server.")

args = parser.parse_args()

# ---- Import Server code ---- #
import os
import sys

# Add server code to Python PATH for imports.
sys.path.append(args.interop_path)
# Add environment variable to get Django settings file.
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "server.settings")

# Setup Django.
from django.core.wsgi import get_wsgi_application
application = get_wsgi_application()

from auvsi_suas.models.gps_position import GpsPosition
from auvsi_suas.models.waypoint import Waypoint
from auvsi_suas.models.fly_zone import FlyZone
from auvsi_suas.models.stationary_obstacle import StationaryObstacle
from auvsi_suas.models.mission_config import MissionConfig

# ----------------------------- #

with open(args.planfile) as f:
    plan = json.load(f)


# Verify that this is actually a planfile
try:
    if plan["fileType"] != "Plan":
        sys.exit("File is not a .plan file!")
except KeyError:
    sys.exit("File is not a .plan file!")

# Convert waypoints
waypoints = []
for c,waypoint in enumerate(plan["mission"]["items"]):
    if waypoint["type"] != "SimpleItem":
        print("WARN: This script only supports simple MISSION_ITEMS at the moment. Encountered a ComplexItem.")
    altitude = waypoint["Altitude"]

    if altitude == 0: continue

    latitude = waypoint["params"][4]
    longitude = waypoint["params"][5]

    waypoints.append(Waypoint(latitude=latitude,
                              longitude=longitude,
                              altitude_msl=altitude*3.28084,
                              order=c+1))
    waypoints[-1].save()

print(waypoints)

# Convert geofence
flyzone_points = []
for c,flyzone in enumerate(plan["geoFence"]["polygons"]):
    if not flyzone["inclusion"]: continue
    for point in flyzone["polygon"]:
        flyzone_points.append(Waypoint(latitude=point[0],
                                       longitude=point[1],
                                       altitude_msl=0,
                                       order=c+1))
        flyzone_points[-1].save()

print(flyzone_points)

max_height = args.max_height
min_height = args.min_height
if max_height <= min_height:
    print("Max Height must be greater than Min Height! Defaulting to 100-750ft MSL range.")
    max_height = 750
    min_height = 100

# ---------------------------- #
mission = MissionConfig()
gpos = GpsPosition(latitude=plan["mission"]["plannedHomePosition"][0],
                   longitude=plan["mission"]["plannedHomePosition"][1])
gpos.save()
mission.home_pos = gpos
mission.lost_comms_pos = gpos
mission.emergent_last_known_pos = gpos
mission.off_axis_odlc_pos = gpos
mission.air_drop_pos = gpos
mission.ugv_drive_pos = gpos

# All foreign keys must be defined before the first save.
# All many-to-many must be defined after the first save.
mission.save()

bounds = FlyZone(altitude_msl_min=min_height, altitude_msl_max=750)
bounds.save()

if len(flyzone_points) == 0:
    # Add default bounds
    for c, transform in enumerate([(1, 1), (1, -1), (-1, 1), (-1, -1)]):
        point = Waypoint(latitude=gpos.latitude+transform[0],
                         longitude=gpos.longitude+transform[1],
                         altitude_msl=0,
                         order=c)
        point.save()
        bounds.boundary_pts.add(point)
else:
    for point in flyzone_points:
        bounds.boundary_pts.add(point)

bounds.save()
mission.fly_zones.add(bounds)

for point in waypoints:
    mission.mission_waypoints.add(point)

stationary_obstacle = StationaryObstacle(latitude=0,
                                         longitude=0,
                                         cylinder_radius=0,
                                         cylinder_height=0)
stationary_obstacle.save()
mission.stationary_obstacles.add(stationary_obstacle)

search_grid = Waypoint(latitude=0, longitude=0, altitude_msl=0, order=-1)
search_grid.save()
mission.search_grid_points.add(search_grid)

mission.save()

print("added mission with id", mission.id)
