# qgc2interop utility #
This utility converts a QGroundControl .plan file to an Interop Mission.

Usage:
`./convert.sh [planfile] [path_to_interop_server]`

MAKE SURE THE INTEROP SERVER IS NOT RUNNING WHEN YOU RUN THIS COMMAND.
If the interop server was recently running, wait a few seconds between
shutdown and running this command, and wait a few seconds between completion
and starting the server back up.

If you run into an error like `Docker daemon not running`, you may need to
run `convert.sh` with `sudo`.

Questions? Email vwangsf@gmail.com (or use the Discord).
